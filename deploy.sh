echo UPDATE YOUR VERSION NUMBER IF NEEDED
# read
rm OUTPUT -r
rm Arachnophobia.zip
mkdir OUTPUT
mkdir OUTPUT/Arachnophobia
cp -r assets OUTPUT/Arachnophobia
cp manifest.json OUTPUT/Arachnophobia
cp content.json OUTPUT/Arachnophobia
cd OUTPUT
zip -r ../Arachnophobia Arachnophobia
cd ..
rm OUTPUT -r